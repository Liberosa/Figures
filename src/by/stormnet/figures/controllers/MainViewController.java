package by.stormnet.figures.controllers;

import by.stormnet.figures.Utils.Point;
import by.stormnet.figures.geometry.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class MainViewController implements Initializable {
    @FXML
    private javafx.scene.control.TextArea TextArea;
    @FXML
    private Label square;

    private Figure[] figures;
    private int figureType;
    private int figureSize;
    private final int MIN_INTERVAL =10;
    private final int MAX_INTERVAL =101;


    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[]{
                null
        };
    }

    @FXML
    private void onMouseReleased(MouseEvent event) {
    addFigure(createFigure(new Point(event.getX(),event.getY())));
    repaint();
    count(figures);
    print(figures);
    }
    private void count(ISquarable[] isquarable){
        double counter = 0.0;
        for (ISquarable iSquarable : isquarable) {
            counter+=iSquarable.squarable();
        }
        square.setText(String.valueOf(counter));


    }
    private void print(IPrintable[] ar){
        StringBuilder sb = new StringBuilder();
        for (IPrintable iPrintable : ar) {
            sb.append(iPrintable.printable()).append("\n\n");
        }
        TextArea.clear();
        TextArea.setText(sb.toString());
    }

    private Figure createFigure(Point point) {
        Random rnd = new Random(System.currentTimeMillis());
        figureType = rnd.nextInt(3);
        figureSize = rnd.nextInt(MAX_INTERVAL) + MIN_INTERVAL;
        switch (figureType){
            case Figure.FIGURE_TYPE_RECT:
                return new Rectangle(point, figureSize, figureSize);
            case Figure.FIGURE_TYPE_TRIANGLE:
                return new Triangle(point, figureSize);
            case Figure.FIGURE_TYPE_CIRCLE:
                int diameter = figureSize; // That it was clear why do we the division into 2..
                return new Circle(point, diameter/2);
            default:
                System.out.println("figure is not found (MainViewController.createFigure)");
                return new Circle(point, 30);
        }

    }

    private void addFigure(Figure figure) {
        if (figures[figures.length - 1] == null) {
            figures[figures.length - 1] = figure;
            return;
        }
        Figure[] tmp = new Figure[figures.length + 1];
        int i = 0;
        for (; i < figures.length; i++) {
            tmp[i] = figures[i];
        }
        tmp[i] = figure;
        figures = tmp;
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (Figure figure : figures) {
            if (figure == null)
                continue;
            figure.draw(canvas.getGraphicsContext2D());
        }
    }

}


