package by.stormnet.figures.geometry;

import by.stormnet.figures.Utils.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Triangle extends Figure  {
    private double baseWidth;


    private Triangle(Point centerPoint) {
        super(Figure.FIGURE_TYPE_TRIANGLE, centerPoint);
    }

    public Triangle(Point centerPoint, double baseWidth) {
        this(centerPoint);
        this.baseWidth = baseWidth;
    }

    public double getBaseWidth() {
        return baseWidth;
    }

    public void setBaseWidth(double baseWidth) {
        this.baseWidth = baseWidth;
    }

    @Override
    public void draw(GraphicsContext gc) {
        if (gc == null)
            return;
        double[] vertexXCoordinates = new double[]{
                centerPoint.x,

                centerPoint.x + baseWidth / 2,
                centerPoint.x - baseWidth / 2,
        };

        double[] vertexYCoordinates = new double[]{
                centerPoint.y - baseWidth/2,
                centerPoint.y + baseWidth/2,
                centerPoint.y + baseWidth/2,

        };
        gc.setStroke(Color.CORNFLOWERBLUE);
        gc.setLineWidth(2.0);
        gc.strokePolygon(vertexXCoordinates, vertexYCoordinates, 3);

    }

    @Override
    public String printable() {
        return String.format("This is a triangle.Basewidth = %.2f",baseWidth);

    }

    @Override
    public double squarable() {
        double hp = (baseWidth + baseWidth + baseWidth) / 2;
        double geron = hp * (hp - baseWidth) * (hp - baseWidth) * (hp - baseWidth);
        return Math.sqrt(geron);


    }


}
