package by.stormnet.figures.geometry;

import by.stormnet.figures.Utils.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rectangle extends Figure  {
    private double width;
    private double height;


    private Rectangle(Point centerPoint) {
        super(FIGURE_TYPE_RECT, centerPoint);
    }

    public Rectangle(Point centerPoint, double width, double height) {
        this(centerPoint);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public void draw(GraphicsContext gc) {
        if (gc == null)
            return;
        gc.setLineWidth(2.0);
        gc.setStroke(Color.BLUEVIOLET);

        gc.strokeRect(centerPoint.x - width / 2, centerPoint.y - height / 2, width, height);

    }

    @Override
    public String printable() {
        return String.format("This is a rectangle.Width = %.2f,height = %.2f",width,height);

    }

    @Override
    public double squarable() {

        return width * height;


    }
}