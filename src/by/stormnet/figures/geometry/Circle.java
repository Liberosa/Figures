package by.stormnet.figures.geometry;

import by.stormnet.figures.Utils.Point;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure  {
    private double radius;



    private Circle(Point centerPoint) {
        super(FIGURE_TYPE_CIRCLE, centerPoint);
    }

    public Circle(Point centerPoint, double radius) {
        this(centerPoint);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        if (gc == null)
            return;
        gc.setStroke(Color.AQUAMARINE);
        gc.setLineWidth(2.0);
        gc.strokeOval(centerPoint.x - radius, centerPoint.y - radius, radius * 2, radius * 2);
    }

    @Override
    public String printable() {
        return String.format("This is a circle.Diameter=%.2f",2*radius);

    }

    @Override
    public double squarable() {

        return Math.PI * Math.pow(radius, 2);


    }


}
