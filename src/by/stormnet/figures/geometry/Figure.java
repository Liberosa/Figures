package by.stormnet.figures.geometry;

import by.stormnet.figures.Utils.Point;
import javafx.scene.canvas.GraphicsContext;

public abstract class Figure implements IPrintable, ISquarable {
    public static final int FIGURE_TYPE_RECT = 0;
    public static final int FIGURE_TYPE_CIRCLE = 1;
    public static final int FIGURE_TYPE_TRIANGLE = 2;

    private int type;
    protected Point centerPoint;

    public Figure(int type, Point centerPoint) {
        this.type = type;
        this.centerPoint = centerPoint;
    }

    public int getType() {
        return type;
    }

    public Point getCenterPoint() {

        return centerPoint;
    }

    public void setCenterPoint(Point centerPoint) {
        this.centerPoint = centerPoint;
    }

    public abstract void draw(GraphicsContext gc);
}

