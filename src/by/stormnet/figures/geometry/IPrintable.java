package by.stormnet.figures.geometry;

public interface IPrintable {
    String printable();
}
